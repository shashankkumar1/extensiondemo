'use strict';

const express = require('express');

const router = express.Router();

router.get('/test-api', (req, res, next) => {
    res.json({
        "hello": "hello"
    });
});

// Get applications list
// router.get('/applications', async function view(req, res, next) {
//     try {
//         const {
//             platformClient
//         } = req;
//         return res.json(await platformClient.configuration.getApplications({
//             pageSize: 1000,
//             q: JSON.stringify({"is_active": true})
//         }));
//     } catch (err) {
//         next(err);
//     }
// });

router.get('/applications', async function view(req, res, next) {
    console.log("router===>");
    try {
        const {
            platformClient
        } = req;
        const customers = await platformClient
        .application("6310adad88cb4f59dce32b0d")
        .user.getCustomers();
      console.log("customers", customers);
      const products = await platformClient.catalog.getProducts()
      console.log("products",products);
      const brands = await platformClient.configuration.getBrandsByCompany()
      console.log("products",products);
      console.log("brands",brands);
        return res.json(await platformClient.order.getOrderDetails());
    } catch (err) {
        next(err);
    }
});

module.exports = router;